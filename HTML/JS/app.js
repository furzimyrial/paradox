var app = angular.module("myApp",[]);

app.controller("myController",function ($scope) {
  $scope.scrollOffset = 0;
  document.getElementById('container').addEventListener("scroll", function (e) {
    $scope.scrollOffset = e.currentTarget.scrollTop / 3;
    $scope.$apply();

    var x = document.getElementsByClassName('app-content-fade');
    var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
    for (var i = 0; i < x.length; i++) {
      if(i == 1)console.log(e.currentTarget.scrollTop,h,x[i].offsetTop);
      if((e.currentTarget.scrollTop - (h*0.4)) > x[i].offsetTop) {
        document.getElementsByClassName('app-content-fade')[i].setAttribute("style","animation-play-state: running;");
      }
    }
  });

});
